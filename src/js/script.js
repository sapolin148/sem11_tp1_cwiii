document.addEventListener("DOMContentLoaded", function(event) {

    //Un commentaire
    console.log("Ça fonctionne");

    document.getElementById("connexion-lien").addEventListener("click", function (event) {

        event.preventDefault();

        document.querySelector("#connexion-modale").classList.add("open");

    });

    document.getElementById("fermer-connexion-modale").addEventListener("click", function (event) {

        event.preventDefault();

        document.querySelector("#connexion-modale").classList.remove("open");

    });

    document.getElementById("arrow-up").addEventListener("click", function (event) {
        event.preventDefault();


        var intervalId = 0;


        function scrollStep() {

            if (window.pageYOffset === 0) {
                clearInterval(intervalId);
            }
            window.scroll(0, window.pageYOffset - 50);
        }

        function scrollToTop() {

            intervalId = setInterval(scrollStep, 16.66);
        }


        // console.log("salut")
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });


    });

});